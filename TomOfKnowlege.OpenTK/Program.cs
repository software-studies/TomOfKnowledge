﻿#region Подключенные библиотеки

using OpenTK.Graphics.OpenGL;
using OpenTK.Mathematics;
using OpenTK.Windowing.Common;
using OpenTK.Windowing.Desktop;
using System;
using System.Linq;
//using System.Windows.Forms;
using OpenTK.Windowing.GraphicsLibraryFramework;
using System.Collections.Generic;

#endregion

namespace TomOfKnowlege.OpenTK
{
    public class Program
    {
        static int Width { get; set; } = 800;
        static int Height { get; set; } = 800;

        #region Класс для управления окном

        /// <summary>
        /// Класс для управления окном с использованием библиотеки OpenTK (OpenGL)
        /// </summary>
        public class FutureGame : GameWindow
        {
            #region Объявление переменных

            //Значение изначальных цветов фона
            private float colorFrameRedUpdate = 0.001f * 4;
            private float colorFrameGreenUpdate = 0.001f * 4;
            private float colorFrameBlueUpdate = 0.001f * 4;

            //Скорость изменения каждого цвета
            private float colorFrameRed = 153 / 255f;
            private float colorFrameGreen = 50 / 255f;
            private float colorFrameBlue = 204 / 255f;

            private int fps = 0; //--> Кол-во FPS в секунду
            private float fpsTime = 0.0f; //--> Время обновления одного кадра

            private float dAlpha = 0.06f; //--> Скорость изменения угла поворота (в градусах)
            private double Alpha_grad = 0; //--> Текущий угол поворота в градусах

            #endregion

            /// <summary>
            /// Конструктор, принимающий начальные настройки
            /// </summary>
            /// <param name="gameWindowSettings">Первоначальных настройки при создании окна</param>
            /// <param name="nativeWindowaSettings"></param>
            public FutureGame(GameWindowSettings gameWindowSettings, NativeWindowSettings nativeWindowaSettings)
                : base(gameWindowSettings, nativeWindowaSettings)
            {
                //Вывод информции в консольное окно нашего приложения
                // Вывод версии OpenTK
                Console.WriteLine(GL.GetString(StringName.Version));
                Console.WriteLine("\n");

                // Вывод информации о разработчике
                Console.WriteLine(GL.GetString(StringName.Vendor));
                Console.WriteLine("\n");

                // Вывод информации о видеокарте 
                Console.WriteLine(GL.GetString(StringName.Renderer));
                Console.WriteLine("\n");

                // Вывод информации о версии языка OpenTK
                Console.WriteLine(GL.GetString(StringName.ShadingLanguageVersion));

                VSync = VSyncMode.On; //--> Обрезание лишних FPS для окна
                CursorVisible = true; //--> Видим ли курср в окне
            }

            #region Основные функции управления окном

            /// <summary>
            /// При инициализации окна
            /// </summary>
            protected override void OnLoad()
            {
                base.OnLoad();

                //Задание первоначального цвета фона
                //GL.ClearColor(Color4.Chocolate);
                //GL.ClearColor(red: 153 / 255f,
                //              green: 50 / 255f,
                //              blue: 204 / 255f,
                //              alpha: 0.8f);

                // Выключает отображение одной из сторон (для большей производительности)
                GL.Enable(EnableCap.CullFace);
                GL.CullFace(CullFaceMode.Back);

                // Выключает *специальное* отображение одной из сторон
                //GL.PolygonMode(MaterialFace.Back, PolygonMode.Line);
                //GL.PolygonMode(MaterialFace.Front, PolygonMode.Fill);

                //GL.Viewport(0, 0, Width, Height);
                //GL.Ortho(0, Width, Height, 0, -1, 1);

                GL.Rotate(-15, 1.0f, 0.0f, 0.0f); //--> поворачивает все примитивы относительно вектора
                GL.Rotate(15, 0.0f, 1.0f, 0.0f); //--> поворачивает все примитивы относительно вектора
                GL.Rotate(0, 0.0f, 0.0f, 1.0f); //--> поворачивает все примитивы относительно вектора

            }

            /// <summary>
            /// При изменении размеров окна
            /// </summary>
            /// <param name="e"></param>
            protected override void OnResize(ResizeEventArgs e)
            {
                base.OnResize(e);
            }

            /// <summary>
            /// Метод при  обновления кадров в окне<br/> 
            /// <i>(Для расчетов)</i>
            /// Связан с <see cref="OnRenderFrame">функией</see> <i><see cref="OnRenderFrame"/></i>
            /// </summary>
            /// <param name="e"></param>
            protected override void OnUpdateFrame(FrameEventArgs e)
            {
                UpdateFPS(e);
                Diskoteka();
                //UpdateAlpha();

                var key = KeyboardState; //--> Текущая нажатая клавиша

                if (key.IsKeyDown(Keys.Escape)) //--> Обработчик клавиши
                { Close(); }

                if (key.IsKeyDown(Keys.W)) //--> Обработчик клавиши
                {
                    if (gg != 1)
                    { 
                        gg = 1;
                        Alpha_grad = 0;
                    }
                    Alpha_grad -= dAlpha;
                    GL.Rotate(Alpha_grad, 1.0f, 0.0f, 0.0f); //--> поворачивает все примитивы относительно вектора
                }
                if (key.IsKeyDown(Keys.A)) //--> Обработчик клавиши
                {
                    if (gg != 2)
                    {
                        gg = 2;
                        Alpha_grad = 0;
                    }
                    Alpha_grad += dAlpha;
                    GL.Rotate(Alpha_grad, 0.0f, 0.0f, 1.0f); //--> поворачивает все примитивы относительно вектора
                }
                if (key.IsKeyDown(Keys.S)) //--> Обработчик клавиши
                {
                    if (gg != 3)
                    {
                        gg = 3;
                        Alpha_grad = 0;
                    }
                    Alpha_grad += dAlpha;
                    GL.Rotate(Alpha_grad, 1.0f, 0.0f, 0.0f); //--> поворачивает все примитивы относительно вектора
                }
                if (key.IsKeyDown(Keys.D)) //--> Обработчик клавиши
                {
                    if (gg != 4)
                    {
                        gg = 4;
                        Alpha_grad = 0;
                    }
                    Alpha_grad -= dAlpha;
                    GL.Rotate(Alpha_grad, 0.0f, 0.0f, 1.0f); //--> поворачивает все примитивы относительно вектора
                }


                Alpha_grad -= dAlpha;
                GL.Rotate(Alpha_grad, 0.0f, 0.0f, 1.0f); //--> поворачивает все примитивы относительно вектора
                base.OnUpdateFrame(e);
            }
            private int gg = 0;


            /// <summary>
            /// Метод при обновления кадров в окне<br/> 
            /// <i>(Для отрисовки)</i><br/>
            /// Связан с <see cref="OnUpdateFrame">функией</see> <i><see cref="OnUpdateFrame"/></i>
            /// </summary>
            /// <param name="e"></param>
            protected override void OnRenderFrame(FrameEventArgs e)
            {
                // Очищение буффера цвета
                GL.Clear(ClearBufferMask.ColorBufferBit);

                // Изменение цвета фона
                //GL.ClearColor(red: colorFrameRed,
                //              green: colorFrameGreen,
                //              blue: colorFrameBlue,
                //              alpha: 1f);


                GL.PointSize(8f); //--> задаёт размер точки, в пикселях
                GL.LineWidth(1f); //--> задаёт размер линии, в пикселях

                // Начало отрисовки примитивов
                GL.Begin(PrimitiveType.Points);
                {
                    GL.Color3(red: 1f,
                              green: 1f,
                              blue: 1f);
                    GL.Vertex3(.0f, .0f, .0f);
                }
                GL.End();
                //нижняя грань
                GL.Begin(PrimitiveType.TriangleStrip);
                {
                    //Задаёт цвет данной точки
                    //GL.Color3(red: Math.Sin(colorFrameRed),
                    //          green: Math.Sin(colorFrameGreen),
                    //          blue: Math.Sin(colorFrameBlue));
                    //Задаёт координаты данной точки
                    GL.Color3(red: 0.9f,
                              green:0.1f,
                              blue: 0.1f);
                    GL.Vertex3(.5f, -.5f, -0.5);

                    GL.Color3(red: 0.1f,
                              green: 0.1f,
                              blue: 0.9f);
                    GL.Vertex3(.5f, .5f, -0.5);

                    GL.Color3(red: 0.9,
                              green: 0.9,
                              blue: 0.1);
                    GL.Vertex3(-.5f, -.5f, -0.5);

                    GL.Color3(red: 0.1,
                              green: 0.9,
                              blue: 0.9);
                    GL.Vertex3(-.5f, .5f, -0.5);

                    GL.Color3(red: 0.9,
                              green: 0.1,
                              blue: 0.1);
                    GL.Vertex3(-.5f, -.5f, 0.5);

                    GL.Color3(red: 0.1,
                              green: 0.1,
                              blue: 0.9);
                    GL.Vertex3(-.5f, .5f, 0.5);

                    GL.Color3(red: 0.9,
                              green: 0.9,
                              blue: 0.1);
                    GL.Vertex3(.5f, -.5f, 0.5);

                    GL.Color3(red: 0.1,
                              green: 0.9,
                              blue: 0.9);
                    GL.Vertex3(.5f, .5f, 0.5);

                    GL.Color3(red: 0.9,
                             green: 0.1,
                             blue: 0.1);
                    GL.Vertex3(.5f, -.5f, -0.5);

                    GL.Color3(red: 0.1,
                              green: 0.1,
                              blue: 0.9);
                    GL.Vertex3(.5f, .5f, -0.5);
                }
                GL.End(); //--> Конец отрисовки примитивов

                

                //нижняя грань
                GL.Begin(PrimitiveType.TriangleStrip);
                {
                    GL.Color3(red: 0.85,
                              green: 0.15,
                              blue: 0.15);
                    GL.Vertex3(.5f, -.5f, -0.5);

                    GL.Color3(red: 0.85,
                              green: 0.85,
                              blue: 0.15);
                    GL.Vertex3(-.5f, -.5f, -0.5);

                    GL.Color3(red: 0.85,
                              green: 0.85,
                              blue: 0.15);
                    GL.Vertex3(.5f, -.5f, 0.5);

                    GL.Color3(red: 0.85,
                              green: 0.15,
                              blue: 0.15);
                    GL.Vertex3(-.5f, -.5f, 0.5);
                }
                GL.End(); //--> Конец отрисовки примитивов

                //верхняя грань
                GL.Begin(PrimitiveType.TriangleStrip);
                {
                    GL.Color3(red: 0.15,
                              green: 0.85,
                              blue: 0.85);
                    GL.Vertex3(-.5f, .5f, -0.5);

                    GL.Color3(red: 0.15,
                              green: 0.15,
                              blue: 0.85);
                    GL.Vertex3(.5f, .5f, -0.5);

                    GL.Color3(red: 0.15,
                              green: 0.15,
                              blue: 0.85);
                    GL.Vertex3(-.5f, .5f, 0.5);

                    GL.Color3(red: 0.15,
                              green: 0.85,
                              blue: 0.85);
                    GL.Vertex3(.5f, .5f, 0.5);
                }
                GL.End(); //--> Конец отрисовки примитивов


                // Возвращает прерущий баффер преобразования
                // Т.е. отменяет все произведенные изменения, например поворот
                SwapBuffers();
                base.OnRenderFrame(e);

            }

            /// <summary>
            /// Метод обработки действий при закрытии окна
            /// </summary>
            protected override void OnUnload()
            {
                base.OnUnload();
            }

            #endregion

            #region Вспомогательные функции


            /// <summary>
            /// Вычисляет FPS <br/>
            /// <i>и выводит в заглоовок окна</i>
            /// </summary>
            /// <param name="e"></param>
            private void UpdateFPS(FrameEventArgs e)
            {
                fpsTime += (float)e.Time;
                fps++;
                if (fpsTime > 1.0f)
                {
                    Title = $"MyBaby have FPS: {fps}";
                    fps = 0;
                    fpsTime = .0f;
                }
            }

            /// <summary>
            /// Изменения цвета окна в реальном времени
            /// </summary>
            private void Diskoteka()
            {
                if (colorFrameRed > 1f || colorFrameRed < .0f)
                    colorFrameRedUpdate *= -1;
                colorFrameRed += colorFrameRedUpdate;

                if (colorFrameGreen > 1f || colorFrameGreen < .0f)
                    colorFrameGreenUpdate *= -1;
                colorFrameGreen += colorFrameGreenUpdate;

                if (colorFrameBlue > 1f || colorFrameBlue < .0f)
                    colorFrameBlueUpdate *= -1;
                colorFrameBlue += colorFrameBlueUpdate;
            }

            /// <summary>
            /// Изменения угла поворота в реальном времени
            /// </summary>
            private void UpdateAlpha()
            {
                if (Alpha_grad > 360 || Alpha_grad < 360)
                    dAlpha *= -1;
                Alpha_grad += dAlpha;
            }

            #endregion
        }

        #endregion

        #region Управляющая функция приложения

        /// <summary>
        /// Управляющая функция приложения
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            // Задание размеров окна
            int width = Width, height = Height;
            // Задание первоначальных настроек при создании окна
            var windowSettings = new NativeWindowSettings()
            {
                Title = "MyBaby",
                Size = new Vector2i(width, height), //--> Задание размеров окна
                // Задание положения окна
                //Location = new Vector2i(System.Windows.Forms.Screen.AllScreens.Max(s => s.Bounds.Width) / 2 - width / 2,
                //                        System.Windows.Forms.Screen.AllScreens.Max(s => s.Bounds.Height) / 2 - height / 2),
                WindowBorder = WindowBorder.Resizable, //--> Шапка окна
                WindowState = WindowState.Normal, //--> Параметры открытия окна (FullScreen,Minimized)
                StartVisible = true, //--> Видимость окна при открытии
                StartFocused = true, //--> Фокус приложения при открытии

                APIVersion = new Version(4, 0), //--> Версия OpenTK
                API = ContextAPI.OpenGL, //--> Использовние OpenGL в окне
                //Flags = ContextFlags.Default,
                //Profile = ContextProfile.Core
                Profile = ContextProfile.Compatability

            };

            using (FutureGame futureGame = new FutureGame(GameWindowSettings.Default, windowSettings))
            {
                futureGame.Run(); //--> Запуск окна
            }
        }

        #endregion
    }
}
