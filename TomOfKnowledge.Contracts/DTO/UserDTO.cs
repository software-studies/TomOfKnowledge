﻿#region includes



#endregion;

namespace TomOfKnowledge.Contracts.DTO
{
    public class UserDTO
    {
        public UserDTO(string userName, string password, int age)
        {
            UserName = userName;
            Password = password;
            Age = age;
        }

        public string UserName { get; set; }
        public string Password { get; set; }
        public int Age { get; set; }
    }
}
