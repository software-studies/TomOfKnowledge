﻿#region includes



#endregion

namespace TomOfKnowledge.Contracts.DTO
{
    public class UsersDTO
    {

        public UsersDTO(ICollection<UserDTO> users) =>
            CollectionUsers = users;

        public ICollection<UserDTO> CollectionUsers { get; set; }


    }
}
