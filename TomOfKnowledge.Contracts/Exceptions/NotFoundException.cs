﻿#region includes


#endregion;


namespace TomOfKnowledge.Contracts.Exceptions
{
    /// <summary>
    /// исключение, возникающее, когда элемент не найден
    /// </summary>
    //создается для того, чтобы не привязываться к реализации, не найден может быть файл (FileNotFoundException), запись бд (Null - что-то там ..) и тд.
    public class NotFoundException : Exception
    {
    }
}
