﻿#region includes



#endregion;

namespace TomOfKnowledge.Contracts.Services
{
    public interface ISerializeService
    {
        T? Deserialiaze<T>(string data);

        T? DeserialiazeFile<T>(string filename);

        string Serialize<T>(T data);

        void SerializeFile<T>(T data, string filename);
    }
}
