﻿#region includes



#endregion

using TomOfKnowledge.Contracts.DTO;

namespace TomOfKnowledge.Contracts.Services
{
    public interface IAuthenticationDataService
    {
        /// <summary>
        /// Получает текущий пароль для проверки <br/>
        /// <i>пока получает из файла Json</i>
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        string GetPasswordFromUserName(string userName);

        ICollection<UserDTO>? GetAllUsers();
    }
}
