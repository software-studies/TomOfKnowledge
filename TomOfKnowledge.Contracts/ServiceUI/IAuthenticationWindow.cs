﻿#region includes



#endregion


namespace TomOfKnowledge.Contracts.ServiceUI
{
    public interface IAuthenticationWindow
    {
        void Show();

        bool? ShowDialog();

        void Close();
    }
}
