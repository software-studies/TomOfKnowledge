﻿#region includes

using TomOfKnowledge.Contracts.Services;

#endregion;

namespace TomOfKnowledge.Contracts.ServicesFacade
{
    public interface IServiceFacade_SerializeJson
    {
        string AppDataPath { get; }
        ISerializeService SerializeService { get; }
    }
}
