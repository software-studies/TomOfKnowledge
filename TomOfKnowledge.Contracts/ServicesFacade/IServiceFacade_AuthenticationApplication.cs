﻿#region includes

using TomOfKnowledge.Contracts.Services;

#endregion;

namespace TomOfKnowledge.Contracts.ServicesFacade
{
    public interface IServiceFacade_AuthenticationApplication
    {
        IAuthenticationDataService AuthenticationDataService { get; }
    }
}
