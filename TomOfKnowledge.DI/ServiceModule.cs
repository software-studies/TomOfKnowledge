﻿#region includes

using Ninject;
using Ninject.Parameters;
using System.IO;
using TomOfKnowledge.Authentication;
using TomOfKnowledge.Contracts.Services;
using TomOfKnowledge.Contracts.ServicesFacade;
using TomOfKnowledge.Contracts.ServiceUI;
using TomOfKnowledge.DI.ServicesFacade;
using TomOfKnowledge.UI.View;
using TomOfKnowledge.UI.ViewModel;
using TomOfKnowledge.Utils;

#endregion

namespace TomOfKnowledge.DI
{
    public class ServiceModule
    {
        //пример на Ninject, можно использовать любой контейнер или реализовать через фабрики
        private readonly StandardKernel kerner;
        private readonly string appDataPath;

        private static readonly Lazy<ServiceModule> lazy =
            new(() => new ServiceModule());

        public static ServiceModule GetInstance() =>
            lazy.Value;
        public ServiceModule()
        {
            appDataPath = Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                    "TomOfKnowledge");

            if (Directory.Exists(appDataPath) == false)
                Directory.CreateDirectory(appDataPath);

            kerner = new StandardKernel();

            //Main
            kerner.Bind<IAuthenticationDataService>().To<AuthenticationData>().InSingletonScope();
            kerner.Bind<IAuthenticationApplication>().To<AuthenticationViewModel>().InSingletonScope();
            kerner.Bind<IAuthenticationWindow>().To<AuthenticationWindow>().InSingletonScope();

            //Utils
            kerner.Bind<ISerializeService>().To<JsonSerialiazer>().InSingletonScope();

            //Facades
            kerner.Bind<IServiceFacade_SerializeJson>().To<ServiceFacade_SerializeJson>().InSingletonScope();
            kerner.Bind<IServiceFacade_AuthenticationApplication>().To<ServiceFacade_AuthenticationApplication>().InSingletonScope();

        }

        private ISerializeService GetISerializeService() =>
            kerner.Get<ISerializeService>();

        private IServiceFacade_SerializeJson GetIServiceFacade_SerializeJson() =>
            kerner.Get<IServiceFacade_SerializeJson>(new ConstructorArgument[]
            {
                new ConstructorArgument("appDataPath", appDataPath),
                new ConstructorArgument("serializeService", GetISerializeService()),
            });

        private IAuthenticationDataService GetAuthenticationDataService() =>
            kerner.Get<IAuthenticationDataService>(new ConstructorArgument[]
            {
                new ConstructorArgument("_src", GetIServiceFacade_SerializeJson()),
            });

        private IServiceFacade_AuthenticationApplication GetServiceFacade_AuthenticationApplication() =>
            kerner.Get<IServiceFacade_AuthenticationApplication>(new ConstructorArgument[]
            {
                new ConstructorArgument("authenticationDataService", GetAuthenticationDataService()),
            });

        public IAuthenticationApplication GetAuthenticationApplication() =>
            GetAuthenticationApplication(GetServiceFacade_AuthenticationApplication());

        public IAuthenticationApplication GetAuthenticationApplication(IServiceFacade_AuthenticationApplication _src) =>
            kerner.Get<IAuthenticationApplication>(new ConstructorArgument[]
            {
                new ConstructorArgument("_src", _src),
            });

        public IAuthenticationWindow GetAuthenticationWindow(IAuthenticationApplication authenticationApplication) =>
            kerner.Get<IAuthenticationWindow>(new ConstructorArgument[]
            {
                new ConstructorArgument("authenticationApplication", authenticationApplication),
            });

        public static Window1 GetWindow1() => new();
    }
}