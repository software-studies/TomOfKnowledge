﻿#region includes

using TomOfKnowledge.Contracts.Services;
using TomOfKnowledge.Contracts.ServicesFacade;

#endregion

namespace TomOfKnowledge.DI.ServicesFacade
{
    internal class ServiceFacade_AuthenticationApplication : IServiceFacade_AuthenticationApplication
    {
        public IAuthenticationDataService AuthenticationDataService { get; }

        public ServiceFacade_AuthenticationApplication(IAuthenticationDataService authenticationDataService)
        {
            AuthenticationDataService = authenticationDataService;
        }
    }
}
