﻿#region includes

using TomOfKnowledge.Contracts.Services;
using TomOfKnowledge.Contracts.ServicesFacade;

#endregion

namespace TomOfKnowledge.DI.ServicesFacade
{
    internal class ServiceFacade_SerializeJson : IServiceFacade_SerializeJson
    {
        public string AppDataPath { get; }
        public ISerializeService SerializeService { get; }
        public ServiceFacade_SerializeJson(string appDataPath, ISerializeService serializeService)
        {
            AppDataPath = appDataPath;
            SerializeService = serializeService;
        }
    }
}
