﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace TomOfKnowledge.UI.View.Converters
{
    public class PasswordConverter : IValueConverter
    {
        private readonly string passwordChar = "🖕";
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string result = string.Empty;
            if (value is string password)
            {
                foreach (char c in password)
                {
                    result += passwordChar;
                }
            }
            return result;
        }


        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
