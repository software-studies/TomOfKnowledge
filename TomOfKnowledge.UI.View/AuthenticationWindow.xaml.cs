﻿#region includes

using System.Windows;
using TomOfKnowledge.Contracts.ServiceUI;

#endregion;

namespace TomOfKnowledge.UI.View
{
    /// <summary>
    /// Логика взаимодействия для AuthenticationWindow.xaml
    /// </summary>
    public partial class AuthenticationWindow : Window, IAuthenticationWindow
    {
        public AuthenticationWindow(IAuthenticationApplication authenticationApplication)
        {
            InitializeComponent();
            DataContext = authenticationApplication;
        }
    }
}
