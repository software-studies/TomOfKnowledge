﻿#region includes

using Newtonsoft.Json;
using TomOfKnowledge.Contracts.Services;

#endregion


namespace TomOfKnowledge.Utils
{
    public class JsonSerialiazer : ISerializeService
    {
        public T? Deserialiaze<T>(string data)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(data);
            }
            catch (InvalidOperationException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public T? DeserialiazeFile<T>(string filename)
        {
            if (File.Exists($"{filename}.json"))
            {
                using (StreamReader sr = new($"{filename}.json"))
                {
                    try
                    {
                        return Deserialiaze<T>(sr.ReadToEnd());
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            throw new FileNotFoundException(filename);
        }

        public void SerializeFile<T>(T data, string filename)
        {
            using StreamWriter fs = File.CreateText($"{filename}.json");
            JsonSerializer serializer = new();
            serializer.Serialize(fs, data);
            //using (StreamWriter fs = File.CreateText($"{filename}.json"))
            //{
            //    JsonSerializer serializer = new JsonSerializer();
            //    serializer.Serialize(fs, data);
            //}
        }

        public string Serialize<T>(T data)
        {
            return JsonConvert.SerializeObject(data);
        }
    }
}