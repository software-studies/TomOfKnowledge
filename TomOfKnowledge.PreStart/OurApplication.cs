﻿using TomOfKnowledge.Contracts.ServiceUI;
using TomOfKnowledge.DI;

namespace TomOfKnowledge.PreStart
{
    internal class OurApplication
    {
        private readonly IAuthenticationWindow authenticationWindow;

        public OurApplication()
        {
            ServiceModule serviceModule = ServiceModule.GetInstance();
            var AuthenticationApplication = serviceModule.GetAuthenticationApplication();
            AuthenticationApplication.OnDoingSomethinsEvent += AuthenticationApplication_OnDoingSomethinsEvent;
            authenticationWindow = serviceModule.GetAuthenticationWindow(AuthenticationApplication);
            authenticationWindow.Show();
        }

        private void AuthenticationApplication_OnDoingSomethinsEvent(object? sender, Contracts.Enum.AuthenticationResult e)
        {
            //ServiceModule serviceModule1 = ServiceModule.GetInstance();
            //var a = ServiceModule.GetWindow1();
            //a.Show();

            authenticationWindow.Close();
            string[] aa = new string[1];
            TomOfKnowlege.OpenTK.Program.Main((string[])aa);

            //MessageBox.Show("We did it");
        }
    }
}
