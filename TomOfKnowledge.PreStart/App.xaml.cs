﻿#region includes

using System.Windows;
using TomOfKnowledge.Contracts.ServiceUI;
using TomOfKnowledge.DI;

#endregion

namespace TomOfKnowledge.PreStart
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            _ = new OurApplication();
        }
    }
}