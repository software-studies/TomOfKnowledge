﻿#region includes

using Microsoft.Toolkit.Mvvm.Input;
using System.Collections.ObjectModel;
using System.Windows.Input;
using TomOfKnowledge.Contracts.DTO;
using TomOfKnowledge.Contracts.Services;
using TomOfKnowledge.UI.ViewModel.Abstract;
using TomOfKnowledge.UI.ViewModel.Interfaces;

#endregion

namespace TomOfKnowledge.UI.ViewModel.Controls
{
    /// <summary>
    /// Пример класса с некими настройками
    /// </summary>
    public class SettingViewModel : Notifier, IContent
    {
        private readonly IAuthenticationDataService src;

        /// <summary>
        /// Оповещат об отмене (как аналок enum Result в диалоге), здесь реализовано как событие для краткости
        /// </summary>
        public EventHandler OnCancel;
        /// <summary>
        /// Аналог кнопки ОК в диалоге
        /// </summary>
        //можно было вы не передавать параметры в событии, а обращаться на прямую к Items, но такой подход более гибкий 
        public EventHandler<IEnumerable<UserDTO>> OnCompleted;

        //здесь можно использовать 2 подхода:
        //1 - передаем сервис и дальше работаем с ним в классе, например, обновление данных из сервиса с учетом меняющихся паарметров:
        //	поиск, постраничный вывод и тд, или без параметров, но если все равно нужно взаимодействие с сервисом
        //2 - это передача коллекции в конструкторе (вместо сервиса), используется в более универсальных классах, когда работа с сервисом 
        //	или не нужна, или усложняет код
        public SettingViewModel(IAuthenticationDataService _src)
        {
            src = _src;
            OnCompleted += (a, e) => { };
            OnCancel += (a, e) => { };
            UpdateCommand?.Execute(null);
        }

        private ObservableCollection<CheckableItemViewModel<UserDTO>> items = new();
        /// <summary>
        /// Элементы RevitObjectDTO для выбора
        /// </summary>
        public ObservableCollection<CheckableItemViewModel<UserDTO>> Items
        {
            get => items;
            set
            {
                items = value;
                OnPropertyChanged();
            }
        }

        #region Function

        public void Clear() { return; }

        #endregion


        #region Command

        public ICommand UpdateCommand => updateCommand ??
                    (updateCommand = new RelayCommand(() =>
                    {

                        var AllUsers = src.GetAllUsers()?.Select(x => new CheckableItemViewModel<UserDTO>(x)).Where(x => x != null);
                        if (AllUsers != null)
                            Items = new ObservableCollection<CheckableItemViewModel<UserDTO>>(AllUsers);
                    }));

        public ICommand CancelCommand => cancelCommand ??
                    (cancelCommand = new RelayCommand(() =>
                    {
                        OnCancel?.Invoke(this, new EventArgs());
                    }));

        public ICommand ConfirmCommand => confirmCommand ??
                    (confirmCommand = new RelayCommand(() =>
                    {
                        OnCompleted?.Invoke(this, Items.Where(x => x.IsChecked).Select(x => x.Item).ToList());
                    }));
        #endregion


        #region garbage

        private ICommand updateCommand;
        private ICommand cancelCommand;
        private ICommand confirmCommand;

        #endregion
    }
}
