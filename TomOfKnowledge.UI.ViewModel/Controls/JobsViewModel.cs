﻿#region includes

using Microsoft.Toolkit.Mvvm.Input;
using System.Windows.Input;
using TomOfKnowledge.Contracts.Services;
using TomOfKnowledge.UI.ViewModel.Abstract;
using TomOfKnowledge.UI.ViewModel.Interfaces;

# endregion

namespace TomOfKnowledge.UI.ViewModel.Controls
{
    public class JobsViewModel : Notifier, IContent
    {
        public EventHandler OnCancel;
        public EventHandler OnCompleted;

        public JobsViewModel(IAuthenticationDataService _src)
        {
            OnCancel += (a, e) => { };
            OnCompleted += (a, e) => { };
        }

        #region Function

        public void Clear() { return; }

        #endregion

        #region Command

        public ICommand CancelCommand => cancelCommand ??= new RelayCommand(() =>
                   {
                       OnCancel?.Invoke(this, new EventArgs());
                   });

        public ICommand ConfirmCommand => confirmCommand ??= new RelayCommand(() =>
                    {
                        OnCompleted?.Invoke(this, new EventArgs());
                    });

        #endregion

        #region garbage

        private ICommand cancelCommand;
        private ICommand confirmCommand;

        #endregion
    }
}
