﻿#region includes

using Microsoft.Toolkit.Mvvm.Input;
using System.Windows.Input;
using TomOfKnowledge.Contracts.Services;
using TomOfKnowledge.UI.ViewModel.Abstract;
using TomOfKnowledge.UI.ViewModel.Interfaces;

#endregion

namespace TomOfKnowledge.UI.ViewModel.Controls
{
    public class StartAuthenticationViewModel : Notifier, IContent
    {
        private readonly IAuthenticationDataService src;
        public EventHandler OnRegister;
        public EventHandler<bool> OnCompleted;

        public StartAuthenticationViewModel(IAuthenticationDataService _src)
        {
            src = _src;
            OnRegister += (a, e) => { };
            OnCompleted += (a, e) => { };
        }

        public string UserName
        {
            get => userName; set
            {
                userName = value;
                OnPropertyChanged(nameof(UserName));

            }
        }

        public string Password
        {
            get => password; set
            {
                password = value;
                OnPropertyChanged(nameof(Password));

            }
        }

        #region Function

        public void Clear() { return; }

        #endregion


        #region Command

        public ICommand RegisterCommand => registerCommand ??= new RelayCommand(() =>
                    {
                        OnRegister?.Invoke(this, new EventArgs());
                    });


        public ICommand EnterAccCommand => enterAccCommand ??= new RelayCommand(() =>
                    {
                        var correctPassword = src.GetPasswordFromUserName(UserName);
                        if (correctPassword != null)
                        {
                            if (correctPassword == password)
                            {
                                OnCompleted?.Invoke(this, true);
                            }
                            else
                            {
                                OnCompleted?.Invoke(this, false);
                            }
                        }
                        else
                        {
                            OnCompleted?.Invoke(this, false);
                        }

                    });

        #endregion

        #region garbage

        private string userName = String.Empty;
        private string password = String.Empty;
        private ICommand registerCommand;
        private ICommand enterAccCommand;

        #endregion
    }
}
