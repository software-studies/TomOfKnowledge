﻿#region includes

using TomOfKnowledge.UI.ViewModel.Abstract;

#endregion

namespace TomOfKnowledge.UI.ViewModel
{
    /// <summary>
    /// Класс - композиция для выбора элемента
    /// Используется в полиморфных списках, таблицах или в "одиночку"
    /// </summary>
    /// <typeparam name="T">Выбираемый элемент</typeparam>
    //композиция - альтернатива плоскому объекту (создать класс со свойством IsChecked, потом от него наследовать)
    //подход более гибкий, но менее управляемый, в качестве плоского объекта чаще всего используется набор: bool IsChecked, T Id, string Name
    //в данном примере выбрана композиция, т.к. используем DTO вместо ViewModel 
    public class CheckableItemViewModel<T> : Notifier
    {
        private bool isChecked;
        /// <summary>
        /// признак выбранного элемента
        /// </summary>
        public bool IsChecked
        {
            get => isChecked;
            set { isChecked = value; OnPropertyChanged(); }
        }


        private T item;
        /// <summary>
        /// Элемент выбора
        /// </summary>
        public T Item
        {
            get => item;
            set { item = value; OnPropertyChanged(); }
        }

        public CheckableItemViewModel(T _item)
        {
            item = _item;
        }

    }
}
