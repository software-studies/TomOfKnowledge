﻿#region includes

using Microsoft.Toolkit.Mvvm.Input;
using System.Windows.Input;
using TomOfKnowledge.Contracts.Enum;
using TomOfKnowledge.Contracts.Services;
using TomOfKnowledge.Contracts.ServicesFacade;
using TomOfKnowledge.Contracts.ServiceUI;
using TomOfKnowledge.UI.ViewModel.Abstract;
using TomOfKnowledge.UI.ViewModel.Controls;
using TomOfKnowledge.UI.ViewModel.Interfaces;
using TomOfKnowledge.UI.ViewModel.ServiceModuleUI;

#endregion


namespace TomOfKnowledge.UI.ViewModel
{
    public class AuthenticationViewModel : Notifier, IAuthenticationApplication
    {
        public event EventHandler<AuthenticationResult> OnDoingSomethinsEvent;
        private readonly IAuthenticationDataService src;
        private readonly ServiceModuleViewModule srcUI;
        public AuthenticationViewModel(IServiceFacade_AuthenticationApplication _src)
        {
            this.OnDoingSomethinsEvent += (object? sender, AuthenticationResult e) => { };
            this.src = _src.AuthenticationDataService;
            this.srcUI = ServiceModuleViewModule.GetInstance();
            Initialize();
        }

        private void Initialize()
        {
            MainContent = CreateStartAuthenticationContent();
        }

        private IContent CreateStartAuthenticationContent()
        {
            var app = srcUI.GetStartAuthenticationViewModel(src);
            app.OnRegister += (o, e) => MainContent = CreateRegistrationContent();
            app.OnCompleted += (o, e) =>
            {
                if (e == true)
                {
                    MainContent = CreateJobsContent();
                }
                else { }
            };
            return app;
        }
        private IContent CreateSettingsContent()
        {
            var app = srcUI.GetSettingViewModel(src);
            app.OnCancel += (o, e) => MainContent = CreateStartAuthenticationContent();
            app.OnCompleted += (o, e) =>
            {
                //do somethins
                MainContent = CreateJobsContent();
            };
            return app;
        }

        private IContent CreateJobsContent()
        {
            var app = srcUI.GetJobsViewModel(src);
            app.OnCancel += (o, e) => MainContent = CreateStartAuthenticationContent();
            app.OnCompleted += (o, e) =>
            {
                //do somethins
                MainContent = CreateSettingsContent();
            };
            return app;
        }

        private IContent CreateRegistrationContent()
        {
            var app = srcUI.GetRegistrationViewModel(src);
            app.OnCancel += (o, e) => MainContent = CreateStartAuthenticationContent();
            app.OnCompleted += (o, e) =>
            {
                //do somethins
                MainContent = CreateSettingsContent();
            };
            return app;
        }

        /// <summary>
        /// контент главного представления
        /// </summary>
        //пример использования полиморфного поведения при отображении интерфейса вместо
        //очень распространенного приема - создание множества классов viewmodel и управление отображением через visible
        public IContent MainContent
        {
            get => mainContent;
            set
            {
                PreviewContent = mainContent;

                mainContent = value;
                OnPropertyChanged(nameof(MainContent));
            }
        }

        public IContent PreviewContent
        {
            get => previewContent;
            set
            {
                previewContent = value;
                OnPropertyChanged(nameof(PreviewContent));
            }
        }


        #region Command

        /// <summary>
        /// пример команды,когда нужно что-то сообщить внешнему подписчику
        /// </summary>
        public ICommand ExternalCommand => externalCommand ??
                    (externalCommand = new RelayCommand(() =>
                    {
                        //для примера реализовано в команде, может быть в любом другом методе
                        OnDoingSomethinsEvent?.Invoke(this, AuthenticationResult.Success);
                    }));


        #endregion



        #region garbage

        private IContent mainContent;
        private IContent previewContent;

        private ICommand externalCommand;

        #endregion
    }
}