﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TomOfKnowledge.Contracts.Services;
using TomOfKnowledge.UI.ViewModel.Controls;

namespace TomOfKnowledge.UI.ViewModel.ServiceModuleUI
{
    internal class ServiceModuleViewModule
    {
        private static readonly Lazy<ServiceModuleViewModule> lazy = new(() => new ServiceModuleViewModule());

        public static ServiceModuleViewModule GetInstance() => lazy.Value;

        public JobsViewModel GetJobsViewModel(IAuthenticationDataService _src) => jobsViewModel ??= new(_src);
        JobsViewModel jobsViewModel;

        public SettingViewModel GetSettingViewModel(IAuthenticationDataService _src) => settingViewModel ??= new(_src);
        SettingViewModel settingViewModel;

        public StartAuthenticationViewModel GetStartAuthenticationViewModel(IAuthenticationDataService _src) => startAuthenticationViewModel ??= new(_src);
        StartAuthenticationViewModel startAuthenticationViewModel;

        public RegistrationViewModel GetRegistrationViewModel(IAuthenticationDataService _src) => registrationViewModel ??= new(_src);
        RegistrationViewModel registrationViewModel;
    }
}
