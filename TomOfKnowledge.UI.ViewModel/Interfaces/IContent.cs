﻿#region includes



#endregion

namespace TomOfKnowledge.UI.ViewModel.Interfaces
{
    public interface IContent
    {
        //Очищает значения в интерфейсе, которые должны быть очищены
        void Clear();
    }
}
