﻿#region includes

using System.ComponentModel;
using System.Runtime.CompilerServices;

#endregion

namespace TomOfKnowledge.UI.ViewModel.Abstract
{
    public abstract class Notifier : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;
        /// <summary>
        /// Реализация интерфейса INotifyPropertyChanged
        /// </summary>
        public virtual void OnPropertyChanged([CallerMemberName] string prop = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
    }
}
