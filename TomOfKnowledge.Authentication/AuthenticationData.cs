﻿#region includes

using TomOfKnowledge.Contracts.DTO;
using TomOfKnowledge.Contracts.Exceptions;
using TomOfKnowledge.Contracts.Services;
using TomOfKnowledge.Contracts.ServicesFacade;

#endregion

namespace TomOfKnowledge.Authentication
{
    public class AuthenticationData : IAuthenticationDataService
    {
        private readonly ISerializeService serializer;
        private readonly string appPath;

        public AuthenticationData(IServiceFacade_SerializeJson _src)
        {
            //можно сделать как аргумент конструктора
            appPath = _src.AppDataPath;
            serializer = _src.SerializeService;

            UsersDTO users = new(new List<UserDTO>()
            {
                new UserDTO("JopaYTiRex", "hufhkjdc", 22),
                new UserDTO("MrFirdavs", "sjdnvfsdk", 23),
                new UserDTO("Renat", "dsbfdnjfs", 21),
                new UserDTO("Svetlana", "SvetO4ka", 21),
            });

            SaveConfiguration(users, "AllUsers");
        }

        public ICollection<UserDTO>? GetAllUsers() => ReadConfiguration("AllUsers").CollectionUsers;


        public string GetPasswordFromUserName(string userName)
        {
            var a = ReadConfiguration("AllUsers");
            var b = a.CollectionUsers.FirstOrDefault(x => x.UserName == userName);
            if (b != null)
                return b.Password;
            else
                return String.Empty;
        }


        public UsersDTO? ReadConfiguration(string name)
        {
            try
            {
                //работая с адаптером мы не знаем json это или xml
                //обратите внимание, что ссылки на json вообще нет в сборке, а тем более на конкретную версию.
                string pathNameFile = Path.Combine(appPath, name);
                return serializer.DeserialiazeFile<UsersDTO>(pathNameFile);
            }
            catch (FileNotFoundException)
            {
                //уходим от конкретной реализации, выдаем исключение из DTO
                throw new NotFoundException();
            }
            catch
            {
                throw;
            }

        }

        public void SaveConfiguration(UsersDTO configuration, string name)
        {
            try
            {
                string pathNameFile = Path.Combine(appPath, name);
                serializer.SerializeFile(configuration, pathNameFile);
            }
            catch
            {
                //здесь аналогичным образом с ReadConfiguration можно сделать AccessException, DirectoryNotFoundException и тд
                throw;
            }

        }
    }
}

